import { actionTypes } from "./index";
export const getTags = data => ({
  payload: data,
  type: actionTypes.GET_TAGS_INIT
});

export const getPosts = data => ({
  payload: data,
  type: actionTypes.GET_POSTS_INIT
});

export const getSinglePost = data => ({
  payload: data,
  type: actionTypes.GET_SINGLE_POST_INIT
});

export const addPost = data => ({
  payload: data,
  type: actionTypes.ADD_POST_INIT
});

export const updatePost = data => ({
  payload: data,
  type: actionTypes.UPDATE_POST_INIT
});

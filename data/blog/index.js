export const actionTypes = {
  GET_TAGS_INIT: "GET_TAGS_INIT",
  GET_TAGS_SUCCESS: "GET_TAGS_SUCCESS",
  GET_TAGS_ERROR: "GET_TAGS_ERROR",
  GET_POSTS_INIT: "GET_POSTS_INIT",
  GET_POSTS_SUCCESS: "GET_POSTS_SUCCESS",
  GET_POSTS_ERROR: "GET_POSTS_ERROR",
  GET_SINGLE_POST_INIT: "GET_SINGLE_POST_INIT",
  GET_SINGLE_POST_SUCCESS: "GET_SINGLE_POST_SUCCESS",
  GET_SINGLE_POST_ERROR: "GET_SINGLE_POST_ERROR",
  ADD_POST_INIT: "ADD_POST_INIT",
  ADD_POST_SUCCESS: "ADD_POST_SUCCESS",
  ADD_POST_ERROR: "ADD_POST_ERROR",
  UPDATE_POST_INIT: "UPDATE_POST_INIT",
  UPDATE_POST_SUCCESS: "UPDATE_POST_SUCCESS",
  UPDATE_POST_ERROR: "UPDATE_POST_ERROR"
};
const initialState = {
  list: null,
  postData: null,
  isPublishing: false,
  tags: null
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.GET_TAGS_INIT:
      return { ...state, inProgress: true };
    case actionTypes.GET_TAGS_SUCCESS:
      return { ...state, tags: payload };
    case actionTypes.GET_POSTS_INIT:
      return { ...state, inProgress: true };
    case actionTypes.GET_POSTS_SUCCESS:
      return { ...state, inProgress: false, list: payload };
    case actionTypes.GET_SINGLE_POST_INIT:
      return { ...state, inProgress: true };
    case actionTypes.GET_SINGLE_POST_SUCCESS:
      return { ...state, inProgress: false, postData: payload };
    case actionTypes.ADD_POST_INIT:
      return { ...state, isPublishing: true };
    case actionTypes.ADD_POST_SUCCESS:
      return { ...state, isPublishing: false };
    case actionTypes.ADD_POST_ERROR:
      return { ...state, isPublishing: false };
    case actionTypes.UPDATE_POST_INIT:
      return { ...state, isPublishing: true };
    case actionTypes.UPDATE_POST_SUCCESS:
      return { ...state, isPublishing: false };
    case actionTypes.UPDATE_POST_ERROR:
      return { ...state, isPublishing: false };
    default:
      return state;
  }
};

import { put, takeEvery } from "redux-saga/effects";
import { actionTypes } from "./index";
import * as API from "../api/posts";
import { navigate } from "@reach/router";

function* getTags() {
  try {
    const tagsData = yield API.getTags();
    yield put({ type: actionTypes.GET_TAGS_SUCCESS, payload: tagsData.body });
  } catch (err) {
    yield put({ type: actionTypes.GET_TAGS_ERROR, payload: err });
  }
}

function* getPosts() {
  try {
    const postsData = yield API.getAllPosts();
    yield put({ type: actionTypes.GET_POSTS_SUCCESS, payload: postsData.body });
  } catch (e) {
    console.log(e);
    yield put({ type: actionTypes.GET_POSTS_ERROR });
  }
}
function* getPost({ payload }) {
  try {
    const postData = yield API.getSinglePost(payload);
    yield put({
      type: actionTypes.GET_SINGLE_POST_SUCCESS,
      payload: postData.body
    });
  } catch ({ response }) {
    console.log(response);
    if (response && response.status === 404) {
      navigate("/404");
    }
    yield put({
      type: actionTypes.GET_SINGLE_POST_ERROR
    });
  }
}
function* addPost({ payload }) {
  yield API.addPost(payload);
  yield put({ type: actionTypes.ADD_POST_SUCCESS });
}
function* updatePost({ payload }) {
  yield API.updatePost(payload);
  yield put({ type: actionTypes.UPDATE_POST_SUCCESS });
}
export default function() {
  return [
    takeEvery(actionTypes.GET_TAGS_INIT, getTags),
    takeEvery(actionTypes.GET_POSTS_INIT, getPosts),
    takeEvery(actionTypes.GET_SINGLE_POST_INIT, getPost),
    takeEvery(actionTypes.ADD_POST_INIT, addPost),
    takeEvery(actionTypes.UPDATE_POST_INIT, updatePost)
  ];
}

import { actionTypes } from "./index";

export const register = (data) => ({
  payload: data,
  type: actionTypes.REGISTER_INIT,
});

export const login = (data) => ({
  payload: data,
  type: actionTypes.LOGIN_INIT,
});

export const logout = (data) => ({
  payload: data,
  type: actionTypes.LOGOUT_INIT,
});

export const activate = (data) => ({
  payload: data,
  type: actionTypes.ACTIVATE_INIT,
});

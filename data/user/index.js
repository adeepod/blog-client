export const actionTypes = {
  LOGIN_INIT: "LOGIN_INIT",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_ERROR: "LOGIN_ERROR",
  LOGOUT_INIT: "LOGOUT_INIT",
  LOGOUT_SUCCESS: "LOGOUT_SUCCESS",
  LOGOUT_ERROR: "LOGOUT_ERROR",
  REGISTER_INIT: "REGISTER_INIT",
  REGISTER_SUCCESS: "REGISTER_SUCCESS",
  REGISTER_ERROR: "REGISTER_ERROR",
  ACTIVATE_INIT: "ACTIVATE_INIT",
  ACTIVATE_SUCCESS: "ACTIVATE_SUCCESS",
  ACTIVATE_ERROR: "ACTIVATE_ERROR"
};
const initialState = {
  inProgress: false,
  isLoggedIn: false,
  username: null,
  signup: {
    error: null,
    inProgress: false
  }
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.LOGIN_INIT:
      return { ...state, inProgress: true };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        inProgress: false,
        isLoggedIn: true,
        username: payload
      };
    case actionTypes.LOGOUT_INIT:
      return { ...state };
    case actionTypes.LOGOUT_SUCCESS:
      return {
        ...state,
        inProgress: false,
        isLoggedIn: false,
        username: null
      };
    case actionTypes.REGISTER_INIT:
      return { ...state, inProgress: true };
    case actionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        inProgress: false
      };
    case actionTypes.REGISTER_ERROR:
      return { ...state, inProgress: false };
    default:
      return state;
  }
};

import { put, takeEvery, takeLeading } from "redux-saga/effects";
import { actionTypes } from "./index";
import * as API from "../api/users";
import { navigate } from "@reach/router";

function* login({ payload }) {
  const { username, password, callback } = payload;
  try {
    const loginData = yield API.login({
      username: username,
      password: password
    });
    const { token } = loginData.body;
    if (token) {
      window.localStorage.setItem("token", token);
      yield put({ type: actionTypes.LOGIN_SUCCESS, payload: username });
      callback(null);
    }
  } catch (e) {
    const { error } = e.response.body;
    error && callback(error);
    yield put({ type: actionTypes.LOGIN_ERROR });
  }
}

function* register({ payload }) {
  const { username, email, password, callback } = payload;
  try {
    const registerReq = yield API.register({
      username: username,
      password: password,
      email: email,
      fullname: ""
    });
    if (registerReq.status === 201) {
      yield put({ type: actionTypes.REGISTER_SUCCESS });
      callback({ error: null, success: true });
      console.log("created");
    }
  } catch (e) {
    yield put({ type: actionTypes.REGISTER_ERROR });
    const { error } = e.response.body;
    error && callback(error);
    //console.log(e);
  }
  /*const { token } = loginData.body;
  if (token) {
    window.localStorage.setItem("token", token);
    yield put({ type: actionTypes.LOGIN_SUCCESS, payload: username });
    callback();
  }*/
}

function* logout({ payload }) {
  //eslint-disable-next-line
  const { callback } = payload;
  yield window.localStorage.removeItem("token");

  yield put({ type: actionTypes.LOGOUT_SUCCESS });
  //callback();
}

function* activate({ payload }) {
  //const { token } = payload;
  try {
    const activation = yield API.activate({ token: payload });
    console.log(activation);
    yield put({ type: actionTypes.ACTIVATE_SUCCESS });
    navigate("/login", { state: { activated: true } });
  } catch (e) {
    console.log(e);
    yield put({ type: actionTypes.ACTIVATE_ERROR });
  }
}
export default function() {
  return [
    takeLeading(actionTypes.LOGIN_INIT, login),
    takeLeading(actionTypes.REGISTER_INIT, register),
    takeEvery(actionTypes.LOGOUT_INIT, logout),
    takeLeading(actionTypes.ACTIVATE_INIT, activate)
  ];
}

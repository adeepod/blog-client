import request from "superagent";
const BASE = process.env.API_URL;

const getToken = () => localStorage.getItem("token") || "";
export const getSinglePost = id => request.get(`${BASE}posts/${id}`);

export const getAllPosts = () => request.get(`${BASE}posts/`);

export const getTags = () => request.get(`${BASE}tags/`);

export const addPost = val =>
  request
    .post(`${BASE}posts/`)
    .set("x-access-token", getToken())
    .send(val);
export const updatePost = val =>
  request
    .patch(`${BASE}posts/${val.id}`)
    .set("x-access-token", getToken())
    .send(val.data);

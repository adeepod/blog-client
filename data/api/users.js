import request from "superagent";

const BASE = process.env.API_URL;

export const login = (val) => request.post(`${BASE}login/`).send(val);

export const register = (val) => request.post(`${BASE}signup/`).send(val);

export const activate = (val) => request.post(`${BASE}activate/`).send(val);

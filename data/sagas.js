import { all } from "redux-saga/effects";
import userSaga from "./user/saga";
import blogSaga from "./blog/saga";

export default function* rootSaga() {
  yield all([...userSaga(), ...blogSaga()]);
}

const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const environ = require("./environment");
const common = require("./webpack.common");
const path = require("path");

module.exports = env =>
  merge(common, {
    mode: "development",
    output: {
      path: path.join(__dirname, "public/"),
      filename: "bundle.js",
      publicPath: "/"
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: "src/index.html"
      }),
      new webpack.EnvironmentPlugin(environ(env))
    ],
    devServer: {
      contentBase: __dirname,
      compress: true,
      port: 9000,
      historyApiFallback: true
    }
  });

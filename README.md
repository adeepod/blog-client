Demo is served under https://adeepod.gitlab.io/blog-client/

# About

This demo presents basic concepts and usage of tools in blog like application. Main page displays list of articles. Every article is written with use of "rich" text editor. User has ability to log in, write article and tag it with keywords. Posts can be filtered by the tag used.
Authentication mechanism is present - users can register, activate account, login.

### Frontend stack

- React
- Webpack
- Redux Saga
- Styled Components
- Reach Router

### Backend stack

- Node.js
- Express.js
- MongoDB(Mongoose)
- JWT
- Heroku

# Development

Install
`npm install`

To run development mode
`npm run dev`

To run development mode with local version of api
`npm run dev-api-local`

App starts at http://localhost:9000/

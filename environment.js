module.exports = (env = {}) => ({
  NODE_ENV: env.NODE_ENV || "development",
  API_URL: env.LOCAL_API
    ? "http://localhost:3000/"
    : "https://limitless-headland-41720.herokuapp.com/",
  BASE_URL: env.NODE_ENV === "production" ? "/blog-client" : ""
});

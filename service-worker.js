const base = self.location.search.split("=")[1];
var CACHE_NAME = "my-site-cache-v1";
var urlsToCache = ["/", "/static/placeholder.png", "/bundle.js"];

self.addEventListener("install", function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME).then(function(cache) {
      return cache.addAll(urlsToCache.map(url => `${base}${url}`));
    })
  );
});

self.addEventListener("fetch", function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      if (response) {
        return response;
      }

      return fetch(event.request).then(function(response) {
        if (
          event.request.method === "POST" ||
          !response ||
          response.status !== 200 ||
          response.type !== "basic"
        ) {
          return response;
        }
        var responseToCache = response.clone();
        caches.open(CACHE_NAME).then(function(cache) {
          cache.put(event.request, responseToCache);
        });

        return response;
      });
    })
  );
});

import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { Link } from "@reach/router";
import UserArea from "./UserArea";
import { logout } from "../../../data/user/actions";

const TopBar = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-basis: 100%;
  justify-content: space-between;
  align-items: center;
  padding: 0 10px;
  background: #000;
  color: #fff;
  font-size: 18px;
`;
const Nav = styled.nav`
  display: flex;
  height: 40px;
  margin: 0;
  text-decoration: none;
  list-style-type: none;
  & > * {
    display: inline-block;
    padding: 0 8px;
    color: #fff;
  }
`;
const MyLink = styled(Link)`
  text-decoration: none;
  font-weight: normal;
  line-height: 2;
  &[aria-current] {
    color: #61dafb;
    box-shadow: inset 0px -3px 0px 0px #61dafb;
  }
`;
const Bar = ({ user, online, logout }) => (
  <TopBar>
    <Nav>
      <MyLink to={`${process.env.BASE_URL}/`}>Home</MyLink>
      <MyLink to={`${process.env.BASE_URL}/about`}>About</MyLink>
    </Nav>
    <UserArea user={user} online={online} logout={logout} />
  </TopBar>
);

export default connect(
  (state) => ({
    user: state.user,
  }),
  { logout }
)(Bar);

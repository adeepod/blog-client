import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "@reach/router";
import { Visible } from "react-grid-system";
import FlexDiv from "../common/FlexDiv";
import Modal from "../common/Modal";
import LoginForm from "../common/LoginForm";

const NewPost = styled(Link)`
  padding: 5px 8px;
  color: #fff;
  background-color: #32a852;
  text-decoration: none;
  border-radius: 4px;
  margin-right: 5px;
  &:hover {
    background-color: #52b172;
  }
  &::before {
    content: "+";
    ${({ children }) => children && `padding: 0 4px 0 0;`}
  }
  ${({ disabled }) =>
    disabled &&
    `background: #acacac; 
    cursor: not-allowed; 
    &:hover {
      background: #acacac;
    }`}
`;
const UserButton = styled.button`
  color: #fff;
  background: none;
  border: 0;
  cursor: pointer;
  ${({ disabled }) =>
    disabled &&
    `
    cursor: not-allowed;
  `}
`;

const UserArea = ({ user, online, logout }) => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <FlexDiv>
      {user.isLoggedIn && (
        <>
          <Visible sm md lg xl>
            <NewPost disabled={!online} to={`${process.env.BASE_URL}/new`}>
              New post
            </NewPost>
          </Visible>
          <Visible xs>
            <NewPost disabled={!online} to={`${process.env.BASE_URL}/new`} />
          </Visible>
        </>
      )}
      <FlexDiv onClick={() => setIsOpen(!isOpen)} align="center">
        {user.isLoggedIn ? (
          <>
            <UserButton disabled={!online}>{user.username}</UserButton>{" "}
            <UserButton onClick={logout}>Log out</UserButton>
          </>
        ) : (
          <>
            <UserButton disabled={!online}>Log in</UserButton>
          </>
        )}
      </FlexDiv>
      {!user.isLoggedIn ? (
        <Modal open={isOpen} toggle={() => setIsOpen(!isOpen)}>
          <LoginForm onLoginSuccessful={() => setIsOpen(false)} />
        </Modal>
      ) : null}
    </FlexDiv>
  );
};
export default UserArea;

import React from "react";
import styled from "styled-components";
import { Link } from "@reach/router";

import { Editor, EditorState, convertFromRaw } from "draft-js";
import Loader from "./Loader";
import Card from "./common/Card";

const Article = styled(Card.Component)`
  & img {
    width: calc(100% + 20px);
    margin: -10px;
    height: auto;
  }
  & > * {
    display: block;
  }
`;
const Meta = styled.div`
  display: flex;
  flex-basis: 100%;
`;
const TagLabel = styled(Link)`
  padding: 2px 4px;
  margin: 0 2px 0 0;
  background: #e6e6e6;
  border-radius: 5px;
`;
const Tags = ({ list }) => (
  <div style={{ marginTop: "8px" }}>
    {list.map((el) => (
      <TagLabel to={`${process.env.BASE_URL}/tags/${el.name}`} key={el.name}>
        {el.name}
      </TagLabel>
    ))}
  </div>
);
const PostDetails = ({
  title,
  content,
  tags = [],
  created_at,
  author = {},
  allTags,
  loading = true,
}) => {
  if (!content || loading) return <Loader />;
  const contentState = convertFromRaw(JSON.parse(content));
  const editorState = EditorState.createWithContent(contentState);
  return (
    <Article as="article">
      {/* <img src="/static/placeholder.png" /> */}
      <h1>{title}</h1>
      <Editor readOnly editorState={editorState} />
      <Tags
        list={
          allTags
            ? tags.map((tag) => ({
                name: allTags.find((el) => el._id === tag).name,
              }))
            : []
        }
      />
      <Meta>
        <p>
          Posted{" "}
          {(created_at && new Date(created_at).toLocaleDateString()) ||
            "unknown"}{" "}
          by {author ? author.username : "unknown"}
        </p>
      </Meta>
    </Article>
  );
};

export default PostDetails;

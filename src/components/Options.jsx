import React, { useState } from "react";
import { Link } from "@reach/router";
import styled from "styled-components";

const OptionsButton = styled.button`
  letter-spacing: 2px;
  color: #aaa;
  background: none;
  border: none;
  cursor: pointer;
  &:hover {
    background: #efefef;
  }
`;
const OptionsWrapper = styled.div`
  flex-basis: 10px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: flex-end;
`;
const Ul = styled.ul`
  position: absolute;
  padding: 5px 10px;
  border: 1px solid #acacac;

  background-color: #fff;
  list-style: none;
  &:before {
    position: absolute;
    z-index: 2;
    left: 10px;
    top: -6px;
    display: block;
    content: "";
    height: 10px;
    width: 10px;
    border: 1px solid #acacac;
    border-bottom: 0;
    border-right: 0;
    transform: rotate(45deg);
    -webkit-backface-visibility: hidden;
    background-color: #fff;
  }
`;
const StyledLink = styled(Link)`
  font-size: 14px;
  font-weight: normal;
  text-decoration: none;
  color: #000;
`;
const Options = ({ author = {}, user = {}, id }) => {
  const [listOpened, setListOpened] = useState(false);
  return (
    author &&
    author.username === user.username && (
      <OptionsWrapper>
        <OptionsButton onClick={() => setListOpened(!listOpened)} type="button">
          &bull;&bull;&bull;
        </OptionsButton>
        {listOpened && (
          <Ul>
            <li>
              <StyledLink to={`edit/${id}`}>edit</StyledLink>
            </li>
            <li style={{ display: "none" }}>delete</li>
          </Ul>
        )}
      </OptionsWrapper>
    )
  );
};
export default Options;

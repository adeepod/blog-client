import React from "react";
import styled from "styled-components";
import { Link } from "@reach/router";
import Options from "./Options";

const Article = styled.article`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 10px;
  background-color: #fff;
  border: 1px solid #cdcdcd;
  & img {
    width: calc(100% + 20px);
    margin: -10px;
    height: auto;
  }
  & > * {
    display: block;
  }
`;
const TagLabel = styled(Link)`
  padding: 2px 4px;
  margin: 0 2px 0 0;
  background: #e6e6e6;
  border-radius: 5px;
`;
const Header = styled.h1`
  flex-basis: calc(100% - 34px);
  margin-top: 0;
  & * {
    text-decoration: none;
    color: #000;
  }
`;
const P = styled.p`
  flex-basis: 100%;
`;
const Tags = ({ list }) => (
  <div>
    {list.map(el => (
      <TagLabel to={`${process.env.BASE_URL}/tags/${el.name}`} key={el.name}>
        {el.name}
      </TagLabel>
    ))}
  </div>
);
const Post = ({ title, content, tags, _id, author, user }) => {
  return (
    <Article>
      <Header>
        <Link to={`${process.env.BASE_URL}/post/${_id}`}>{title}</Link>
      </Header>
      <Options author={author} user={user} id={_id} />
      <P>
        {JSON.parse(content)
          .blocks.reduce((a, b) => a + " " + b.text, "")
          .substring(0, 200)}
        ...
      </P>
      <Tags list={tags} />
    </Article>
  );
};

export default Post;

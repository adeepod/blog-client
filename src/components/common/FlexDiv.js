import styled from "styled-components";

const FlexDiv = styled.div`
  display: flex;
  ${({ align }) => align && `align-items: ${align};`}
  ${({ justify }) => justify && `justify-content: ${justify};`}
`;
export default FlexDiv;

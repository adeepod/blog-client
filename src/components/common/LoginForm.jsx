import React from "react";
import { connect } from "react-redux";
import { Link } from "@reach/router";
import styled from "styled-components";
import Field from "./Field";
import { login } from "../../../data/user/actions";

const P = styled.p`
  text-align: right;
`;
const LoginForm = ({ onLoginSuccessful, login, activated }) => {
  const [form, setForm] = React.useState({});
  const [errors, setErrors] = React.useState(null);
  return (
    <>
      {activated && <p>Your account is activated</p>}
      <form
        onChange={e => {
          setForm({ ...form, [e.target.name]: e.target.value });
          errors && setErrors(null);
        }}
        onSubmit={e => {
          e.preventDefault();
          console.log(form, login);
          login({
            ...form,
            callback: error => {
              error ? setErrors(error) : onLoginSuccessful();
            }
          });
        }}
      >
        <h1>Login</h1>
        <Field
          errors={errors}
          autoFocus
          type="text"
          name="username"
          placeholder="Username"
        />
        <Field
          errors={errors}
          type="password"
          name="password"
          placeholder="Password"
        />
        <Field errors={errors} type="submit" name="submit" value="Login" />
        <P>
          Do not have an account?{" "}
          <Link to={`${process.env.BASE_URL}/register`}>Sign up</Link>
        </P>
      </form>
    </>
  );
};

export default connect(
  null,
  { login }
)(LoginForm);

import styled, { css } from "styled-components";
const CardCss = css`
  padding: ${({ small }) => (small ? "10px" : "20px")} !important;
  background-color: #fff;
  border: 1px solid #cdcdcd;
`;
const Card = styled.div`
  ${CardCss}
`;
export default { Component: Card, css: CardCss };

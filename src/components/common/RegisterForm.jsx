import React from "react";
import { connect } from "react-redux";
import Field from "./Field";
import { register } from "../../../data/user/actions";

const validate = ({ username, email, password, password2 }) => {
  const valid = password && password === password2 && username && email;
  const errors = { errors: null };
  if (valid) return errors.errors;
  errors.errors = {};
  if (!username) {
    errors.errors = { ...errors.errors, username: "Field required" };
  }
  if (!email) {
    errors.errors = { ...errors.errors, email: "Field required" };
  }
  if (!password) {
    errors.errors = { ...errors.errors, password: "Field required" };
  }
  if (!password2) {
    errors.errors = { ...errors.errors, password2: "Field required" };
  }
  if (password !== password2) {
    errors.errors = {
      ...errors.errors,
      password2: "Password fields must be the same"
    };
  }
  return errors.errors;
};

const RegisterForm = ({ register, loading }) => {
  const [form, setForm] = React.useState({});
  const [errors, setErrors] = React.useState(null);
  const [registerSuccess, setRegisterSuccess] = React.useState(null);
  if (registerSuccess)
    return <h1>Thank you for signing up! Activation email has been sent.</h1>;
  return (
    <form
      onChange={e => {
        setForm({ ...form, [e.target.name]: e.target.value });
        setErrors(null);
      }}
      onSubmit={e => {
        e.preventDefault();
        console.log("submit", form);
        const validation = validate(form);

        if (!validation) {
          console.log(validation);
          const formCopy = Object.assign({}, form);
          delete formCopy["password2"];
          register({
            ...formCopy,
            callback: ({ error, success }) => {
              error ? setErrors(error) : null;
              success ? setRegisterSuccess(true) : null;
            }
          });
        } else {
          setErrors(validation);
          console.log("invalid", validation);
        }
      }}
    >
      <h1>Register</h1>
      <Field
        autoFocus
        errors={errors}
        type="text"
        name="username"
        placeholder="Username"
      />
      <Field errors={errors} type="text" name="email" placeholder="Email" />
      <Field
        errors={errors}
        type="password"
        name="password"
        placeholder="Password"
      />
      <Field
        errors={errors}
        type="password"
        name="password2"
        placeholder="Repeat Password"
      />
      <Field
        errors={errors}
        type="submit"
        name="submit"
        value="Register"
        loading={loading}
      />
    </form>
  );
};

export default connect(
  state => ({
    loading: state.user.inProgress,
    signUpState: state.user.signup
  }),
  { register }
)(RegisterForm);

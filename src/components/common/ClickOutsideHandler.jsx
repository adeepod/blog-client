import React from "react";
import onClickOutside from "react-onclickoutside";
import FlexDiv from "./FlexDiv";

const ClickOutSideHandler = ({ children, fold }) => {
  ClickOutSideHandler.onClickOutside = () => fold();
  return <FlexDiv>{children}</FlexDiv>;
};
const config = {
  handleClickOutside: () => ClickOutSideHandler.onClickOutside
};
const ClickHandler = onClickOutside(ClickOutSideHandler, config);

export default ClickHandler;

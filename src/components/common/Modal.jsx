import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { useLocation } from "@reach/router";
import Card from "./Card";
import { Container } from "react-grid-system";

const ModalFull = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 3;
  background: rgba(0, 0, 0, 0.6);
`;
const ModalCard = styled(Container)`
  position: fixed !important;
  z-index: 3;
  top: 10%;
  left: 0;
  right: 0;
  ${Card.css}
`;
const Close = styled.button`
  position: absolute;
  right: 32px;
  top: 32px;
  width: 32px;
  height: 32px;
  opacity: 0.3;
  border 0;
  background: none;
  
  &:hover {
    opacity: 1;
    cursor: pointer;
  }

  &:before,
  &:after {
    position: absolute;
    top: 0;
    left: 15px;
    content: " ";
    height: 33px;
    width: 2px;
    background-color: #333;
  }
  &:before {
    transform: rotate(45deg);
  }
  &:after {
    transform: rotate(-45deg);
  }
`;
const modalBox = document.getElementById("modal");
const Modal = ({ children, open, toggle }) => {
  const location = useLocation();
  React.useEffect(() => {
    console.log("modal effect");
    open && toggle();
  }, [location]);
  if (!open) return null;
  return ReactDOM.createPortal(
    <div>
      <ModalFull
        onClick={() => {
          toggle();
          console.log("modal click1");
        }}
      />
      <ModalCard>
        {children}
        <Close
          onClick={() => {
            toggle();
            console.log("modal click2");
          }}
        />
      </ModalCard>
    </div>,
    modalBox
  );
};
export default Modal;

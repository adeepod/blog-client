import React from "react";
import styled from "styled-components";
import Loader from "../Loader";
// eslint-disable-next-line
const Input = styled(({ loading, type, invalid, ...other }) => (
  <input type={type} {...other} />
))`
  width: 100%;
  padding: 10px;
  margin: 5px 0;
  border-radius: 0px;
  font-size: 14px;
  text-align: center;
  background-color: #efefef;
  border: 1px solid #cdcdcd;
  ${({ invalid }) => (invalid ? "border: 1px solid red" : "")}
  ${({ type, loading }) =>
    type === "submit"
      ? `color: #fff;
    background-color: #32a852;
    border: 0;
    & +${Loader}::after {
      position: absolute;
      left: calc(50% - 9px);
      top: calc(50% - 10px);
    }
    ${loading ? "color: rgba(0,0,0,0)" : ""}`
      : ""}
`;
const ErrorText = styled.span`
  position: relative;
  top: -7px;
  color: #ff0000;
  font-size: 12px;
`;

const Field = ({ name, errors, loading, ...props }) => {
  if (name && errors) {
    const error = errors[name];
    return (
      <div>
        <Input name={name} {...props} invalid={!!error} />
        {error && <ErrorText>{error}</ErrorText>}
      </div>
    );
  } else {
    return (
      <div style={{ position: "relative" }}>
        <Input name={name} loading={loading} {...props} />
        {loading && <Loader size={18} />}
      </div>
    );
  }
};
export default Field;

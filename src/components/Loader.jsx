import styled from "styled-components";

const LoaderElements = styled.span`
${({ block, size = 46, color = "#61dafb" }) => `
${block ? "display: block; margin: auto;" : "display: inline;"}
  width: ${size * 1.16}px;
  height: ${size * 1.16}px;
  &::after {
    content: " ";
    display: block;
    width: ${size}px;
    height: ${size}px;
    margin: 1px;
    border-radius: 50%;
    border: ${size / 9}px solid ${color};
    border-color: ${color} transparent ${color} transparent;`}
    animation: lds-dual-ring 1.2s linear infinite;
  }
  @keyframes lds-dual-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

export default LoaderElements;

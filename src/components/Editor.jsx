import React from "react";
import {
  Editor,
  EditorState,
  RichUtils,
  convertToRaw,
  convertFromRaw
} from "draft-js";
import Select from "react-select";
import { Container } from "react-grid-system";
import styled from "styled-components";
import { Observer } from "mobx-react";
import "draft-js/dist/Draft.css";
import Loader from "./Loader";

const Controlls = styled.div`
  & button {
    background: none;
    border: 0px;
  }
`;

const StyledEditor = styled(Editor)`
  & [role="textbox"] {
    background-color: #fff;
    border: 1px solid #cccccc;
    border-radius: 3px;
  }
`;
const MyContainer = styled(Container)`
  & [role="textbox"] {
    min-height: 300px;
    background-color: #fff;
    border: 1px solid #cccccc;
    border-radius: 3px;
    padding: 4px;
    & a {
      color: blue;
      text-decoration: underline;
    }
  }
`;
const StyledInput = styled.input`
  display: block;
  width: 100%;
  padding: 4px;
  background-color: #fff;
  border: 1px solid #cccccc;
  border-radius: 3px;
`;
const StyledButton = styled.button`
  display: flex;
  align-items: center;
  padding: 5px 8px;
  margin-top: 5px;
  color: #fff;
  background-color: #32a852;
  text-decoration: none;
  border-radius: 4px;
  box-shadow: none;
  border: 0;
  &:hover {
    background: #52b172;
  }
  ${({ disabled }) =>
    disabled &&
    `
  cursor: not-allowed; 
  &:hover {
    background: #acacac;
  }`}
`;
function MyEditor({
  allTags = [],
  saveChanges,
  content,
  title,
  isPublishing,
  tags
}) {
  if (!content) return <Loader />;
  const contentState = convertFromRaw(JSON.parse(content));
  const [editorState, setEditorState] = React.useState(
    EditorState.createWithContent(contentState)
  );
  const [titleState, setTitleState] = React.useState(title);
  const [selectedTags, setSelectedTags] = React.useState(tags);

  const titleRef = React.useRef(null);
  const editor = React.useRef(null);
  function savePost() {
    saveChanges({
      title: titleRef.current.value,
      content: JSON.stringify(convertToRaw(editorState.getCurrentContent())),
      tags: selectedTags
    });
  }
  function focusEditor() {
    editor.current.focus();
  }
  function _onBoldClick(e) {
    e.preventDefault();
    setEditorState(RichUtils.toggleInlineStyle(editorState, "BOLD"));
  }
  function _onH1Click(e) {
    e.preventDefault();
    setEditorState(RichUtils.toggleBlockType(editorState, "header-one"));
  }
  function _onItalicClick(e) {
    e.preventDefault();
    setEditorState(RichUtils.toggleInlineStyle(editorState, "ITALIC"));
  }

  React.useEffect(() => {
    focusEditor();
  }, []);
  const normalizedSelectTags = selectedTags.map(tag => ({
    label: allTags.find(el => el._id === tag).name,
    value: tag
  }));
  const normalizedAllTags = allTags.map(tag => ({
    label: tag.name,
    value: tag._id
  }));
  return (
    <Observer>
      {() => (
        <MyContainer>
          <label htmlFor="title">Title</label>
          <StyledInput
            required
            type="text"
            name="title"
            ref={titleRef}
            value={titleState}
            onChange={e => setTitleState(e.value)}
          />
          <div onClick={focusEditor}>
            <Controlls>
              <button onMouseDown={e => _onH1Click(e)}>H1</button>
              <button onMouseDown={e => _onItalicClick(e)}>Italic</button>
              <button onMouseDown={e => _onBoldClick(e)}>Bold</button>
            </Controlls>
            <StyledEditor
              ref={editor}
              editorState={editorState}
              onChange={editorState => {
                setEditorState(editorState);
              }}
            />
          </div>
          <label htmlFor="tags">Tags</label>
          <Select
            isMulti
            value={normalizedSelectTags}
            onChange={tags => {
              setSelectedTags(tags.map(tag => tag.value));
            }}
            options={normalizedAllTags ? normalizedAllTags : []}
          />
          <StyledButton disabled={isPublishing} onClick={savePost}>
            Publish {isPublishing && <Loader size={14} color="#fff" />}
          </StyledButton>
        </MyContainer>
      )}
    </Observer>
  );
}

export default MyEditor;

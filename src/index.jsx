import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import rootReducer from "../data";
import { LocationProvider } from "@reach/router";
//import BlogStore from "../data/blogStore";
//import UserStore from "../data/userStore";

import App from "./App";
import io from "socket.io-client";

const socket = io(process.env.API_URL);

socket.on("connect", () => console.log("connect " + socket.id));
socket.on("Notification", (e) => {
  console.log("Notification", e);
  //socket.emit("Hello", e)
});
if ("serviceWorker" in navigator && process.env.NODE_ENV === "production") {
  window.addEventListener("load", function() {
    navigator.serviceWorker
      .register(
        `${process.env.BASE_URL}/service-worker.js?url=${process.env.BASE_URL}`
      )
      .then(
        function(registration) {
          // Registration was successful
          console.log(
            "ServiceWorker registration successful with scope: ",
            registration.scope
          );
        },
        function(err) {
          // registration failed :(
          console.log("ServiceWorker registration failed: ", err);
        }
      );
  });
}
const { store, persistor } = rootReducer();
const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate;
renderMethod(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <LocationProvider>
        <App />
      </LocationProvider>
    </PersistGate>
  </Provider>,
  document.getElementById("app")
);

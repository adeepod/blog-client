import React, { useState } from "react";
import { Router } from "@reach/router";
import styled, { createGlobalStyle } from "styled-components";
import Index from "./pages";
import PostContent from "./pages/post";
import EditPost from "./pages/edit";
import AboutPage from "./pages/about";
import NotFound from "./pages/404";
import NewPostPage from "./pages/new-post";
import Authenticate from "./pages/authenticate";
import Activate from "./pages/activate";
import Bar from "./components/TopBar";

const GlobalStyle = createGlobalStyle`
  body {
    margin:0;
    padding: 0;
    background-color: #efefef;
    font-family: 'Roboto', sans-serif;
    & * {
      box-sizing: border-box
    }
    nav a {
      text-decoration: none;
      color: inherit;
    }
  }
`;
const Page = styled.div`
  padding: 24px 0;
`;
const App = () => {
  const [online, setOnline] = useState(navigator.onLine);
  window.ononline = () => {
    setOnline(true);
  };
  window.onoffline = () => {
    setOnline(false);
  };
  return (
    <>
      <Bar online={online} />
      <Page>
        <Router basepath={process.env.BASE_URL} primary={false}>
          <NotFound default path="404" />
          <Index path="/" />
          <PostContent path="post/:postId" />
          <EditPost path="edit/:postId" />
          <AboutPage path="about" />
          <NewPostPage path="new" />
          <Index path="tags/:tagName" />
          <Authenticate path="login" login />
          <Authenticate path="register" register />
          <Activate path="activate/:token" />
        </Router>
      </Page>
      <GlobalStyle />
    </>
  );
};
export default App;

import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Container } from "react-grid-system";
import PostDetails from "../components/PostDetails";
import { getSinglePost } from "../../data/blog/actions";

const PostContent = ({ blog, postId, getSinglePost }) => {
  useEffect(() => {
    getSinglePost(postId);
  }, [postId]);
  return (
    <Container>
      <PostDetails
        loading={blog.postData && blog.postData._id !== postId}
        {...blog.postData}
        allTags={blog.tags}
      />
    </Container>
  );
};

export default connect(
  state => ({ blog: state.blog }),
  { getSinglePost }
)(PostContent);

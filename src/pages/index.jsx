import React from "react";
import { connect } from "react-redux";
import { Container, Row, Col } from "react-grid-system";
import Post from "../components/Post";
import Loader from "../components/Loader";
import { getPosts, getTags } from "../../data/blog/actions";

class Index extends React.Component {
  componentDidMount() {
    const { getPosts, getTags } = this.props;
    getPosts();
    getTags();
  }
  render() {
    const { blog, tagName, user } = this.props;
    const tags = blog.tags;
    if (!blog.list || !blog.tags) return <Loader />;
    const tag = tags.find(tag => tag.name === tagName);
    const list = tagName
      ? Array.from(blog.list).filter(el => el.tags.includes(tag._id))
      : Array.from(blog.list);

    return (
      <Container>
        {tagName && <h1>Search posts by tag: {tagName}</h1>}
        <Row>
          {list.map((el, i) => (
            <Col
              sm={12}
              key={`col-${el._id}`}
              style={{ marginTop: i === 0 ? "0" : "24px" }}
            >
              <Post
                {...el}
                tags={el.tags.map(tag => ({
                  name: tags.find(el => el._id === tag).name
                }))}
                user={user}
              />
            </Col>
          ))}
        </Row>
      </Container>
    );
  }
}

export default connect(
  state => ({
    blog: state.blog,
    user: state.user
  }),
  {
    getPosts,
    getTags
  }
)(Index);

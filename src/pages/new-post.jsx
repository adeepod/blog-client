import React from "react";
import { connect } from "react-redux";
import { Editor, EditorState, RichUtils, convertToRaw } from "draft-js";
import Select from "react-select";
import { Container } from "react-grid-system";
import styled from "styled-components";
import { Redirect } from "@reach/router";
import { Observer } from "mobx-react";
import "draft-js/dist/Draft.css";
import { addPost } from "../../data/blog/actions";

const Controlls = styled.div`
  & button {
    background: none;
    border: 0px;
  }
`;

const StyledEditor = styled(Editor)`
  & [role="textbox"] {
    background-color: #fff;
    border: 1px solid #cccccc;
    border-radius: 3px;
  }
`;
const MyContainer = styled(Container)`
  & [role="textbox"] {
    min-height: 300px;
    background-color: #fff;
    border: 1px solid #cccccc;
    border-radius: 3px;
    padding: 4px;
    & a {
      color: blue;
      text-decoration: underline;
    }
  }
`;
const StyledInput = styled.input`
  display: block;
  width: 100%;
  padding: 4px;
  background-color: #fff;
  border: 1px solid #cccccc;
  border-radius: 3px;
`;
const StyledButton = styled.button`
  padding: 5px 8px;
  margin-top: 5px;
  color: #fff;
  background-color: #32a852;
  text-decoration: none;
  border-radius: 4px;
  box-shadow: none;
  border: 0;
  &:hover {
    background: #52b172;
  }
  ${({ disabled }) =>
    disabled &&
    `background: #acacac; 
  cursor: not-allowed; 
  &:hover {
    background: #acacac;
  }`}
`;
function MyEditor({ blog, user, addPost }) {
  const [editorState, setEditorState] = React.useState(
    EditorState.createEmpty()
  );
  const [selectedTags, setSelectedTags] = React.useState([]);
  const [createAction, setCreateAction] = React.useState({
    loading: false,
    type: null
  });
  const title = React.useRef(null);
  const editor = React.useRef(null);

  function savePost() {
    setCreateAction({ type: null, loading: true });
    addPost(
      {
        title: title.current.value,
        content: JSON.stringify(convertToRaw(editorState.getCurrentContent())),
        tags: selectedTags.map(tag => tag.value)
      },
      setCreateAction
    );
  }
  function focusEditor() {
    editor.current.focus();
  }
  function _onBoldClick(e) {
    e.preventDefault();
    setEditorState(RichUtils.toggleInlineStyle(editorState, "BOLD"));
  }
  function _onH1Click(e) {
    e.preventDefault();
    setEditorState(RichUtils.toggleBlockType(editorState, "header-one"));
  }
  function _onItalicClick(e) {
    e.preventDefault();
    setEditorState(RichUtils.toggleInlineStyle(editorState, "ITALIC"));
  }

  React.useEffect(() => {
    focusEditor();
  }, []);

  return (
    <Observer>
      {() => (
        <MyContainer>
          {!user.isLoggedIn && <Redirect to="./" />}
          <label htmlFor="title">Title</label>
          <StyledInput required type="text" name="title" ref={title} />
          <div onClick={focusEditor}>
            <Controlls>
              <button onMouseDown={e => _onH1Click(e)}>H1</button>
              <button onMouseDown={e => _onItalicClick(e)}>Italic</button>
              <button onMouseDown={e => _onBoldClick(e)}>Bold</button>
            </Controlls>
            <StyledEditor
              ref={editor}
              editorState={editorState}
              onChange={editorState => {
                setEditorState(editorState);
              }}
            />
          </div>
          <label htmlFor="tags">Tags</label>
          <Select
            isMulti
            value={selectedTags}
            onChange={tags => setSelectedTags(tags)}
            options={
              blog.tags
                ? blog.tags.map(tag => ({
                    label: tag.name,
                    value: tag._id
                  }))
                : []
            }
          />
          <StyledButton disabled={createAction.loading} onClick={savePost}>
            Publish
          </StyledButton>
          {createAction.type && <span>{createAction.type}</span>}
        </MyContainer>
      )}
    </Observer>
  );
}

export default connect(
  state => ({ user: state.user, blog: state.blog }),
  { addPost }
)(MyEditor);

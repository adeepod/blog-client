import React from "react";
import { Match, Redirect } from "@reach/router";
import styled from "styled-components";
const P = styled.p`
  font-size: 45px;
  text-align: center;
  font-weight: bold;
`;
const NotFound = () => {
  return (
    <Match path="/404">
      {props => {
        console.log(props.match, props);
        return props.match ? <P>Not found</P> : <Redirect to="/404" noThrow />;
      }}
    </Match>
  );
};
export default NotFound;

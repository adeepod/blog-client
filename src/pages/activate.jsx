import React from "react";
import { connect } from "react-redux";
import { activate } from "../../data/user/actions";
import LoaderElements from "../components/Loader";

const ActivatePage = ({ token, activate }) => {
  React.useEffect(() => {
    console.log("mounted activate ", token);
    activate(token);
  }, []);
  return <LoaderElements />;
};

export default connect(
  (state) => ({ user: state.user, blog: state.blog }),
  { activate }
)(ActivatePage);

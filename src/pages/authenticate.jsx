import React from "react";
import { Container } from "react-grid-system";
import LoginForm from "../components/common/LoginForm";
import RegisterForm from "../components/common/RegisterForm";
import Card from "../components/common/Card";
import { navigate, useLocation } from "@reach/router";

const Authenticate = ({ login = false, register = false }) => {
  const { state } = useLocation();
  return (
    <Card.Component as={Container}>
      {login && (
        <LoginForm
          onLoginSuccessful={() =>
            state.redirect ? navigate(state.redirect) : navigate("/")
          }
          activated={state.activated}
        />
      )}
      {register && (
        <RegisterForm onLoginSuccessful={() => console.log("Welcome")} />
      )}
    </Card.Component>
  );
};

export default Authenticate;

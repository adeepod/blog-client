import React from "react";
import { Container } from "react-grid-system";
import Card from "../components/common/Card";
import Readme from "../../README.md";

const AboutPage = () => {
  return (
    <Container>
      <Card.Component>
        <div dangerouslySetInnerHTML={{ __html: Readme }} />
      </Card.Component>
    </Container>
  );
};

export default AboutPage;

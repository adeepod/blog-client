import React from "react";
import { connect } from "react-redux";
import { Redirect, useLocation, navigate } from "@reach/router";
import "draft-js/dist/Draft.css";
import Editor from "../components/Editor";
import { updatePost, getSinglePost } from "../../data/blog/actions";
import Loader from "../components/Loader";

const EditPage = ({ blog, user, updatePost, getSinglePost, postId }) => {
  const location = useLocation();
  React.useEffect(() => {
    getSinglePost(postId);
  }, [postId]);

  if (!user.isLoggedIn) {
    navigate("/login", {
      state: { redirect: location.pathname },
      replace: true
    });
    return null;
  }
  return (
    <>
      {user.isLoggedIn &&
        blog.postData &&
        blog.postData.author.username !== user.username && (
          <Redirect to="/" noThrow />
        )}
      {blog.postData && blog.postData._id !== postId ? (
        <Loader />
      ) : (
        <Editor
          allTags={blog.tags}
          saveChanges={data => updatePost({ data: data, id: postId })}
          {...blog.postData}
          isPublishing={blog.isPublishing}
        />
      )}
    </>
  );
};

export default connect(
  state => ({ user: state.user, blog: state.blog }),
  { updatePost, getSinglePost }
)(EditPage);

module.exports = {
  entry: "./src/index.jsx",
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["css-loader"]
      },
      {
        test: /\.md$/,
        use: ["html-loader", "markdown-loader"]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"]
          }
        }
      },
      {
        test: /\.(ttf|eot|woff|woff2|jpg|jpeg|png|svg)$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 8000
          }
        }
      }
    ]
  },
  resolve: {
    extensions: [".js", ".jsx"]
  }
};

const path = require("path");
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const environ = require("./environment");

const common = require("./webpack.common");
console.log(path.join(__dirname, "public/blog-client/"));
module.exports = env =>
  merge(common, {
    mode: "production",
    output: {
      path: path.join(__dirname, "public/"),
      filename: "bundle.js",
      publicPath: "/blog-client/"
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "src/index.html"
      }),
      new webpack.EnvironmentPlugin(environ(env))
    ]
  });

/// <reference types="Cypress" />

context("Login flow", () => {
  beforeEach(() => {
    cy.visit("http://localhost:9000");
  });

  it("Successful login", () => {
    cy.server();
    cy.route({
      method: "POST",
      url: "/login"
    }).as("login");
    cy.get("button")
      .contains("Log in")
      .click();
    cy.get('input[name="username"]').type("Admin");
    cy.get('input[name="password"]').type(`1234{enter}`);
    cy.wait("@login").then(() => {
      cy.get("button").contains("Admin");
      cy.get('input[name="username"]').should("not.exist");
      cy.get('input[name="password"]').should("not.exist");
    });
  });

  it("Failed login", () => {
    cy.server();
    cy.route({
      method: "POST",
      url: "/login"
    }).as("login");
    cy.get("button")
      .contains("Log in")
      .click();
    cy.get('input[name="username"]').type("Admin1");
    cy.get('input[name="password"]').type(`randompassword{enter}`);
    cy.wait("@login").then(xhr => {
      assert.notEqual(xhr.status, 200, "Is not OK");
    });
  });
});
